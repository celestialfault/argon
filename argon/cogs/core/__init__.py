from argon.bot import Argon
from argon.cogs.core.core import Core


async def setup(bot: Argon):
    await bot.add_cog(Core(bot))
