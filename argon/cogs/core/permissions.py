from copy import copy

import discord

from argon import commands, db
from argon.bot import Argon
from argon.cogs.core._shared import _
from argon.utils.chat_formatting import error, info, success, warning


class Permissions:
    bot: Argon

    # region [p]permissions

    @commands.group()
    @commands.guild_only()
    async def permissions(self, ctx: commands.Context):
        """Manage permission settings for the current server"""

    @permissions.command(name="canuse", aliases=["canrun"])
    @commands.has_guild_permissions(administrator=True)
    async def permissions_can_use(
        self, ctx: commands.Context, user: discord.Member, *, command: str
    ):
        """Check if a given user can use a given command"""
        if await self.bot.is_owner(user):
            return await ctx.send(
                info(
                    _("{user} is a bot owner, and as such bypasses most forms of checks.").format(
                        user=user
                    )
                )
            )

        cmd: commands.Command = discord.utils.get(self.bot.walk_commands(), qualified_name=command)
        if not cmd:
            return await ctx.send(warning(_("That's not a valid command.").format()))

        new_msg = copy(ctx.message)
        new_msg.author = user
        new_msg.content = f"{ctx.prefix}{cmd.qualified_name}"
        new_ctx = await self.bot.get_context(new_msg)

        try:
            await cmd.can_run(new_ctx)
        except commands.CheckFailure:
            await ctx.send(info(_("{user} cannot use that command.").format(user=user)))
        else:
            await ctx.send(info(_("{user} can use that command.").format(user=user)))

    @permissions.group(name="role")
    @commands.has_guild_permissions(administrator=True)
    async def permissions_role(self, ctx: commands.Context):
        """Manage role permissions"""

    @permissions_role.command(name="block")
    async def role_block(self, ctx: commands.Context, role: discord.Role, *, command: str):
        """Block members with a given role from using a command"""
        if role == ctx.guild.default_role:
            await ctx.send(
                error(_("You cannot block a command for the server's default role.").format())
            )
            return
        cmd = discord.utils.get(self.bot.walk_commands(), qualified_name=command)
        if not cmd:
            await ctx.send(warning(_("No such command exists.").format()))
            return
        if cmd.root_parent.name in ("permissions", "licenseinfo"):
            await ctx.send(error(_("You cannot block that command.").format()))
            return
        g = await db.Guild.find_one_or_create(ctx.guild)
        if command not in g.command_blocked_roles:
            g.command_blocked_roles[command] = []
        if role.id in g.command_blocked_roles[command]:
            await ctx.send(
                _("{role.mention} is already blocked from using that command.").format(
                    role=role.mention
                )
            )
            return
        g.command_blocked_roles[command].append(role.id)
        await g.set({"command_blocked_roles": g.command_blocked_roles})
        await ctx.send(
            success(_("{role} is now blocked from using that command.").format(role=role.mention))
        )

    @permissions_role.command(name="unblock")
    async def role_unblock(self, ctx: commands.Context, role: discord.Role, *, command: str):
        """Undo a previous command block for a role"""
        cmd = self.bot.get_command(command)
        if not cmd:
            await ctx.send(warning(_("No such command exists.").format()))
            return
        g = await db.Guild.find_one_or_create(ctx.guild)
        if command not in g.command_blocked_roles:
            g.command_blocked_roles[command] = []

        if role.id not in g.command_blocked_roles[command]:
            await ctx.send(
                warning(
                    _("{role} is not currently blocked from using that command.").format(
                        role=role.mention
                    )
                ),
                allowed_mentions=discord.AllowedMentions.none(),
            )
        else:
            g.command_blocked_roles[command].remove(role.id)
            await g.set({"command_blocked_roles": g.command_blocked_roles})
            await ctx.send(
                success(
                    _("{role} is no longer blocked from using that command.").format(
                        role=role.mention
                    )
                )
            )

    # endregion
    # region [p]blocklist

    @commands.group()
    @commands.has_guild_permissions(kick_members=True)
    async def blocklist(self, ctx: commands.Context):
        """Add/remove members from the bot's block list

        Blocked members are prevented from using any bot commands, or otherwise interacting with
        any bot features; they are still subject to moderation features, however.
        """

    @blocklist.command(name="add")
    async def blocklist_add(self, ctx: commands.Context, member: discord.Member):
        """Add a member to the server's blocklist"""
        m = await db.Member.find_one_or_create(member)
        if m.blocked:
            return await ctx.send(
                error(
                    _("{member} is already blocked from using my commands.").format(member=member)
                )
            )
        await m.set({"blocked": True})
        await ctx.send(
            success(_("{member} is now blocked from using my commands.").format(member=member))
        )

    @blocklist.command(name="remove")
    async def blocklist_remove(self, ctx: commands.Context, member: discord.Member):
        """Remove a member from the server's blocklist"""
        m = await db.Member.find_one_or_create(member)
        if not m.blocked:
            return await ctx.send(
                error(_("{member} is not blocked from using my commands.").format(member=member))
            )
        await m.set({"blocked": False})
        await ctx.send(
            success(
                _("{member} is no longer blocked from using my commands.").format(member=member)
            )
        )

    @blocklist.group()
    @commands.is_owner()
    async def blocklist_global(self, ctx: commands.Context):
        """Manage the bot's global blocklist"""

    @blocklist_global.command(name="add")
    async def blocklist_add(self, ctx: commands.Context, user: discord.User):
        """Globally block a user from using this bot"""
        m = await db.User.find_one_or_create(user)
        if m.blocked:
            return await ctx.send(
                error(
                    _("{member} is already globally blocked from using my commands.").format(
                        member=user
                    )
                )
            )
        await m.set({"blocked": True})
        await ctx.send(
            success(
                _("{member} is now globally blocked from using my commands.").format(member=user)
            )
        )

    @blocklist_global.command(name="remove")
    async def global_blocklist_remove(self, ctx: commands.Context, user: discord.User):
        """Remove a previous global block for a user"""
        m = await db.User.find_one_or_create(user)
        if not m.blocked:
            return await ctx.send(
                error(
                    _("{member} is not globally blocked from using my commands.").format(
                        member=user
                    )
                )
            )
        await m.set({"blocked": False})
        await ctx.send(
            success(
                _("{member} is no longer globally blocked from using my commands.").format(
                    member=user
                )
            )
        )

    # endregion
