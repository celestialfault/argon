import discord

from argon import commands, db
from argon.bot import Argon
from argon.cogs.roles._shared import _, log
from argon.utils.chat_formatting import error, success
from argon.utils.embeds import mod_action


class StickyRoles:
    bot: Argon

    @commands.group(aliases=["stickyrole"])
    @commands.has_guild_permissions(manage_roles=True)
    async def stickyroles(self, ctx: commands.Context):
        """Manage sticky roles

        Sticky roles will be re-applied even if the member it is applied to
        leaves and rejoins the server.

        Personal roles assigned with `[p]myrole` will already be considered sticky
        for their assigned user.

        Sticky roles may be removed by simply removing the role from
        the respective member like any other role.
        """

    @stickyroles.command(name="stick", require_var_positional=True)
    async def sticky_stick(
        self, ctx: commands.Context, member: discord.Member, *roles: discord.Role
    ):
        """Forcefully add one or more sticky roles to a member

        The given roles don't have to be marked as sticky with `[p]stickyrole add`.
        """
        m = await db.Member.find_one_or_create(member)
        await m.update({"$push": {"sticky_roles": [*{m.sticky_roles} ^ {x.id for x in roles}]}})
        await member.add_roles(
            *[x for x in roles if x not in member.roles],
            reason=_("Sticky roles added by {admin}").format(admin=str(ctx.author)),
            atomic=False,
        )
        await ctx.send(
            embed=mod_action(
                _(
                    "Added {count, plural, one {# sticky role} other {# sticky roles}} to {member}"
                ).format(count=len(roles), member=member)
            )
        )

    @stickyroles.command(name="unstick", require_var_positional=True)
    async def sticky_unstick(
        self, ctx: commands.Context, member: discord.Member, *roles: discord.Role
    ):
        """Remove one or more sticky roles from a member"""
        m = await db.Member.find_one_or_create(member)
        if to_remove := [x for x in roles if x in member.roles]:
            await m.update({"$pull": {"sticky_roles": {"$in": [x.id for x in to_remove]}}})
            await member.remove_roles(
                *to_remove,
                reason=_("Sticky roles removed by {admin}").format(admin=str(ctx.author)),
                atomic=False,
            )
            await ctx.send(
                embed=mod_action(
                    _(
                        "Removed {count, plural, one {# sticky role} other {# sticky roles}}"
                        " from {member}"
                    ).format(count=len(roles), member=member)
                )
            )
        else:
            await ctx.send(
                error(_("None of those roles could be removed from that member.").format())
            )

    @stickyroles.command(name="add")
    async def sticky_add(self, ctx: commands.Context, *, role: discord.Role):
        """Make a role sticky

        This does not retroactively apply to members who have the given role already.
        """
        r = await db.Role.find_one_or_create(role)
        await r.set({"sticky": True})
        await ctx.send(success(_("{role} is now sticky.").format(role=role)))

    @stickyroles.command(name="remove")
    async def sticky_remove(self, ctx: commands.Context, *, role: discord.Role):
        """Make a role no longer sticky

        This does not retroactively apply to members who have the given role already.
        """
        r = await db.Role.find_one_or_create(role)
        await r.set({"sticky": False})
        await ctx.send(success(_("{role} is no longer sticky.").format(role=role)))

    @stickyroles.command(name="list")
    async def sticky_list(self, ctx: commands.Context):
        """List all roles that are currently sticky"""
        roles = [x async for x in db.Role.find_many({"guild_id": ctx.guild.id, "sticky": True})]
        await ctx.send_pagified(", ".join(f"<@&{x.role_id}>" for x in roles), embed=True)

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        if not member.pending:
            await self.__apply_upon_join(member)

    @commands.Cog.listener()
    async def on_member_update(self, before: discord.Member, after: discord.Member):
        if not isinstance(after, discord.Member):
            return

        if before.pending and not after.pending:
            await self.__apply_upon_join(after)

        if before.roles != after.roles:
            member = await db.Member.find_one_or_create(after)
            sticky = {*member.sticky_roles}

            if added := [x.id for x in after.roles if x not in before.roles]:
                sticky |= {
                    x.role_id
                    async for x in db.Role.find_many({"role_id": {"$in": added}, "sticky": True})
                }

            if removed := {x.id for x in before.roles if x not in after.roles and x.id in sticky}:
                sticky -= removed

            if sticky.difference({*member.sticky_roles}):
                log.debug("Sticky roles for member %r changed, saving to database", after)
                await member.set({"sticky_roles": [*sticky]})

    @staticmethod
    async def __apply_upon_join(member: discord.Member):
        guild = member.guild
        if not guild.me.guild_permissions.manage_roles:
            return
        data = await db.Member.find_one({"user_id": member.id, "guild_id": guild.id})
        if not data:
            return
        my_top_role = guild.me.top_role

        # Load sticky roles
        sticky_roles: list[discord.Role] = [
            r for x in data.sticky_roles if (r := guild.get_role(x)) and r < my_top_role
        ]
        # Add the member's personal role if they have one
        if (personal_role := guild.get_role(data.personal_role)) and personal_role < my_top_role:
            sticky_roles.append(personal_role)
        # If they're muted, reapply that as well; this doesn't apply on timeout-based mutes
        if data.muted:
            gdata = await db.Guild.find_one_or_create(guild)
            if (muted_role := guild.get_role(gdata.mute_role)) and muted_role < my_top_role:
                sticky_roles.append(muted_role)

        if sticky_roles:
            to_add = [x for x in sticky_roles if x not in member.roles]
            await member.add_roles(*to_add, reason=_("Applying roles upon join").format())
