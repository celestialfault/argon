from argon.bot import Argon
from argon.cogs.reminders.core import Reminders
from argon.utils import SLASH_GUILD


async def setup(bot: Argon):
    await bot.add_cog(Reminders(bot), guild=SLASH_GUILD)
