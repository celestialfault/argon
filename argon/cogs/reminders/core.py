from __future__ import annotations

import logging
from datetime import datetime, timedelta

import discord
from beanie import PydanticObjectId
from discord import app_commands
from discord.ext.tasks import loop

from argon import commands, db
from argon.bot import Argon
from argon.cogs.reminders.modal import CreateReminderModal, RescheduleView
from argon.cogs.reminders.shared import _, scheduler
from argon.translations import Humanize
from argon.utils import slash_guild
from argon.utils.chat_formatting import success, warning
from argon.utils.time import TimeDelta

log = logging.getLogger("argon.reminders")


@_.cog()
class Reminders(commands.Cog):
    def __init__(self, bot: Argon):
        super().__init__()
        self.bot = bot
        self.scheduler = scheduler
        self.scheduler.callback = self._scheduler_callback
        self.schedule_timer.start()

        @bot.tree.context_menu(name="Remind me")
        @app_commands.allowed_contexts(guilds=True, dms=True, private_channels=True)
        @app_commands.allowed_installs(guilds=True, users=True)
        @slash_guild
        @_.interaction
        async def create_from_message(interaction: commands.Interaction, message: discord.Message):
            modal = CreateReminderModal(ephemeral=True)
            modal.context_message = message
            await interaction.response.send_modal(modal)

    def cog_unload(self) -> None:
        self.scheduler.stop_all()
        self.schedule_timer.cancel()

    @commands.Cog.listener()
    @_.interaction
    async def on_interaction(self, interaction: commands.Interaction):
        # This could probably be replaced with a singleton persistent view, but due to the
        # use of translation APIs, doing so isn't completely feasible, even if for the time
        # being we only support English.
        if (
            interaction.type != discord.InteractionType.component
            or (interaction.data or {}).get("custom_id", "") != "reminder$reschedule_sent"
        ):
            return

        modal = CreateReminderModal()
        # Remove the reminder header
        modal.text.default = "\n".join(interaction.message.embeds[0].description.split("\n")[2:])
        await interaction.response.send_modal(modal)

    async def _scheduler_callback(self, reminder_id: PydanticObjectId):
        reminder = await db.Reminder.get(reminder_id)
        if not reminder:
            log.warning(
                "Scheduler called for a reminder with the ID %r to be sent,"
                " but no such reminder exists",
                reminder_id,
            )
            return
        try:
            user = self.bot.get_user(reminder.user_id) or await self.bot.fetch_user(
                reminder.user_id
            )
        except discord.NotFound:
            log.warning("Can't find user %r", reminder.user_id)
            await reminder.delete()
            return
        try:
            await user.send(
                embed=discord.Embed(
                    description=_(
                        "**\N{WAVING HAND SIGN} You asked me to remind you of this {delta}**"
                        "\n\n{reminder}"
                    ).format(delta=Humanize(reminder.created_at, "R"), reminder=reminder.text),
                    colour=discord.Colour.blurple(),
                ),
                view=RescheduleView(context_message=reminder.context_message),
            )
        except discord.Forbidden:
            log.warning(
                "Failed to send reminder to %r - they likely have us blocked or turned off DMs,"
                " not attempting to re-send",
                user,
            )
        except discord.DiscordServerError:
            log.exception(
                "Discord encountered an error while attempting to send a reminder to %r,"
                " trying again in 15 minutes",
                user,
            )
            await reminder.set({"due": datetime.utcnow() + timedelta(minutes=15)})
            self.scheduler.add(reminder.id, reminder.due)
            return
        except discord.HTTPException:
            log.exception("Failed to send reminder %r to user, not attempting to re-send", reminder)
        await reminder.delete()

    @loop(minutes=5)
    async def schedule_timer(self):
        await self.bot.wait_until_ready()
        now = datetime.utcnow()
        # noinspection PyTypeChecker
        async for reminder in db.Reminder.find_many(
            # not ignoring cache can occasionally send double reminders if the cache
            # fails to remove the now deleted reminder for whatever reason, so just
            # ignore it given we're only doing this once every few minutes, and we
            # want the most recent list of reminders to begin with
            {"due": {"$lt": now + timedelta(minutes=15)}},
            ignore_cache=True,
        ):
            if reminder.id not in self.scheduler:
                log.debug("Creating scheduled task for %r", reminder)
                self.scheduler.add(reminder.id, reminder.due)

    @commands.hybrid_group(fallback="create")
    @app_commands.describe(
        remind_in="How long to send this reminder in, e.g. '2 days'", text="The reminder to send"
    )
    @app_commands.allowed_contexts(guilds=True, dms=True, private_channels=True)
    @app_commands.allowed_installs(guilds=True, users=True)
    @slash_guild
    async def remind(self, ctx: commands.Context, remind_in: TimeDelta, *, text: str):
        """Create a reminder for future you"""
        if len(text) > 1500:
            await ctx.send(
                _("The given reminder text must be at most 1500 characters in length").format(),
                ephemeral=True,
            )
            return

        reminder = db.Reminder(
            text=text,
            due=datetime.utcnow() + remind_in,
            user_id=ctx.author.id,
            created_at=datetime.utcnow(),
        )
        await reminder.save()
        await ctx.send(
            success(_("Okay, I'll remind you of that at {due, timestamp}").format(due=reminder.due))
        )

    @remind.command(name="list")
    async def remind_list(self, ctx: commands.Context):
        """List all of your currently active reminders"""
        from .reminder_list import ReminderList

        reminders = await db.Reminder.find_many(
            {"user_id": ctx.author.id}, ignore_cache=True
        ).to_list()
        if not reminders:
            await ctx.send(
                warning(_("You don't have any active reminders!").format()), ephemeral=True
            )
            return

        menu = ReminderList(
            reminders=reminders, user=ctx.author, original_interaction=ctx.interaction
        )
        await ctx.send(**await menu.send_kwargs())
