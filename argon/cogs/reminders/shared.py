from argon.translations import Translator
from argon.utils.scheduler import Scheduler
from argon.utils.time import FutureTime

_ = Translator(__file__)
scheduler = Scheduler()
ReminderTime = FutureTime().min_time("30 minutes")
