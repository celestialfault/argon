from __future__ import annotations

import discord

from argon import db
from argon.bot import Argon
from argon.cogs.selfassign.message_types.core import RoleMessage
from argon.cogs.selfassign.shared import _, log
from argon.translations import Humanize
from argon.utils.chat_formatting import error

ADD_FALLBACK = "\N{HEAVY PLUS SIGN}"
REMOVE_FALLBACK = "\N{HEAVY MINUS SIGN}"


class RoleSelect(discord.ui.Select):
    __slots__ = ("guild", "message", "original", "add_emoji", "remove_emoji")

    def __init__(
        self,
        *args,
        guild: discord.Guild,
        message: SelectRoleMessage,
        original: discord.Interaction,
        emojis: dict,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.guild = guild
        self.message = message
        self.original = original
        self.add_emoji = emojis.get("add", ADD_FALLBACK)
        self.remove_emoji = emojis.get("remove", REMOVE_FALLBACK)

    def _gen_message(
        self,
        *,
        added: list[discord.Role],
        removed: list[discord.Role],
        cannot_add: list[discord.Role],
        cannot_remove: list[discord.Role],
    ) -> list[str]:
        message = []
        if added or removed:
            message.append("")
        if added:
            message.append(
                _("{emoji} **Added:** {added}").format(
                    added=Humanize([x.mention for x in added]), emoji=self.add_emoji
                )
            )
        if removed:
            message.append(
                _("{emoji} **Removed:** {removed}").format(
                    removed=Humanize([x.mention for x in removed]), emoji=self.remove_emoji
                )
            )

        if cannot_add or cannot_remove:
            message.append("")
        if cannot_add:
            message.append(
                _(
                    "\N{WARNING SIGN} **The following {count, plural, one {role} other {roles}}"
                    " can only be removed** \N{EM DASH} {roles}"
                ).format(count=len(cannot_add), roles=Humanize([x.mention for x in cannot_add]))
            )
        if cannot_remove:
            message.append(
                _(
                    "\N{WARNING SIGN} **The following {count, plural, one {role} other {roles}}"
                    " can only be added** \N{EM DASH} {roles}"
                ).format(
                    count=len(cannot_remove), roles=Humanize([x.mention for x in cannot_remove])
                )
            )

        return message

    async def callback(self, interaction: discord.Interaction):
        await interaction.response.defer(ephemeral=True, thinking=True)
        all_roles = self.message.all_roles()
        roles: list[discord.Role] = [self.guild.get_role(int(x)) for x in self.values]
        added = []
        removed = []
        cannot_add = []
        cannot_remove = []

        # Make sure we have the latest roles list before continuing, to avoid
        # stepping on a moderator's toes if they're unfortunate enough to add/remove roles from
        # a member while they're using a role select menu
        new_roles = interaction.guild.get_member(interaction.user.id).roles.copy()
        for r in all_roles:
            if r not in new_roles and r in roles:
                if self.message.role_type(r) == "remove":
                    cannot_add.append(r)
                    continue
                new_roles.append(r)
                added.append(r)
            if r in new_roles and r not in roles:
                if self.message.role_type(r) == "add":
                    cannot_remove.append(r)
                    continue
                new_roles.remove(r)
                removed.append(r)

        if new_roles != interaction.user.roles:
            try:
                await interaction.user.edit(
                    roles=new_roles,
                    reason=f"Roles modified from select menu in #{interaction.channel.name}",
                )
            except discord.HTTPException:
                log.exception("Failed to modify roles for member %r", interaction.user)
                await interaction.followup.send(
                    ephemeral=True,
                    content=error(
                        _(
                            "Failed to update roles; please ask an admin for assistance"
                            " if this persists."
                        ).format()
                    ),
                )
                return

        for option in self.options:
            option.default = bool(discord.utils.get(new_roles, id=int(option.value)))
        await self.original.edit_original_response(
            content=self.message.select_message(interaction, roles=new_roles), view=self.view
        )

        # discord has apparently started to consider editing the original message as not
        # responding to the interaction, causing it to show as if it 'failed', so we get to
        # just send an extra response instead...
        await interaction.followup.send(
            "\n".join(
                self._gen_message(
                    added=added,
                    removed=removed,
                    cannot_add=cannot_add,
                    cannot_remove=cannot_remove,
                )
            )
            or error(_("Failed to update any roles").format())
        )


class SelectRoleMessage(RoleMessage):
    __slots__ = ()
    TYPE = "select"

    async def _create_select_view(self, interaction: discord.Interaction) -> discord.ui.View:
        """Create a new view for use in a user-specific select menu interaction"""
        guild = self.message.guild
        view = discord.ui.View()
        select = RoleSelect(
            max_values=1 if self.mutually_exclusive else len(self.roles),
            min_values=0,
            guild=guild,
            message=self,
            original=interaction,
            emojis=(await db.Guild.find_one_or_create(interaction.guild)).select_emojis,
        )

        # Avoid erroring if the given interaction user already has more than one role configured
        # on this message while roles on this message are mutually exclusive
        add_defaults = (
            not self.mutually_exclusive
            or len([x for x in self.all_roles() if x in interaction.user.roles]) <= 1
        )
        for rid, role in self.roles.items():
            r = guild.get_role(rid)
            select.add_option(
                label=role["label"],
                description=role.get("description"),
                emoji=role["emoji"],
                default=r in interaction.user.roles and add_defaults,
                value=str(rid),
            )
        view.add_item(select)
        return view

    def select_message(
        self, interaction: discord.Interaction, roles: list[discord.Role] = None
    ) -> str:
        all_roles = self.all_roles()
        if roles is None:
            roles = interaction.user.roles
        has = [x.mention for x in all_roles if x in roles]
        missing = [x.mention for x in all_roles if x not in roles]

        return (
            _("**Your current role:** {role}").format(role=next(iter(has), _("*none*")))
            if self.mutually_exclusive
            else _(
                "**Roles you already have:** {roles}\n**Roles you do *not* have**: {missing}"
            ).format(
                roles=Humanize(has) if has else _("*none*"),
                missing=Humanize(missing) if missing else _("*none*"),
            )
        )

    @classmethod
    def load(
        cls, bot: Argon, data: db.Message, *, webhook: discord.Webhook = None
    ) -> SelectRoleMessage:
        self = cls(
            discord.PartialMessage(channel=bot.get_channel(data.channel_id), id=data.message_id),
            webhook=webhook,
        )
        return self.reload(bot, data)

    def reload(self, bot: Argon, data: db.Message) -> SelectRoleMessage:
        self.mutually_exclusive = data.mutually_exclusive
        self.roles = {x["id"]: x for x in data.roles}
        view = self.view = discord.ui.View(timeout=None)

        async def callback(interaction: discord.Interaction):
            await interaction.response.send_message(
                ephemeral=True,
                content=self.select_message(interaction),
                view=await self._create_select_view(interaction),
            )

        button = discord.ui.Button(
            custom_id=f"role_{data.message_id}_openselect",
            label=data.select_button.get("message", "Click to assign roles"),
            emoji=data.select_button.get("emoji"),
            style=getattr(
                discord.ButtonStyle,
                data.select_button.get("style", "grey"),
                discord.ButtonStyle.grey,
            ),
        )
        button.callback = callback
        view.add_item(button)
        return self
