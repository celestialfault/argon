from __future__ import annotations

from typing import ClassVar, Literal, Type

import discord

from argon import db
from argon.bot import Argon
from argon.db import ButtonStyleType, SelfAssignRole
from argon.utils import MISSING


class RoleMessage:
    __slots__ = ("message", "view", "roles", "mutually_exclusive", "webhook")
    TYPE: ClassVar[str] = NotImplemented
    MESSAGE_TYPES: ClassVar[dict[str, Type[RoleMessage]]] = {}

    def __init__(
        self, message: discord.Message | discord.PartialMessage, *, webhook: discord.Webhook = None
    ):
        self.message = message
        self.view = discord.ui.View(timeout=None)
        self.roles: dict[int, SelfAssignRole] = {}
        self.mutually_exclusive = False
        self.webhook = webhook

    def __init_subclass__(cls, **kwargs):
        if cls.TYPE == NotImplemented:
            return
        cls.MESSAGE_TYPES[cls.TYPE] = cls

    def role_type(self, role: discord.Role) -> Literal["add", "remove", "both"]:
        """Convenience alias for getting the add/remove type of a role"""
        return self.roles[role.id].get("type", "both")

    def all_roles(self) -> list[discord.Role]:
        """Returns a list of all configured roles on this message"""
        guild = self.message.guild
        return [r for x in self.roles.keys() if (r := guild.get_role(x))]

    @classmethod
    def load(cls, bot: Argon, data: db.Message, *, webhook: discord.Webhook = None) -> RoleMessage:
        raise NotImplementedError

    def reload(self, bot: Argon, data: db.Message) -> RoleMessage:
        raise NotImplementedError

    async def save(self) -> None:
        """Save the current message to Mongo"""
        data = await db.Message.find_one_or_create(self.message)
        await data.set(
            {"roles": [*self.roles.values()], "mutually_exclusive": self.mutually_exclusive}
        )

    async def update(self) -> None:
        """Update the underlying message"""
        if self.webhook is not None:
            await self.webhook.edit_message(self.message.id, view=self.view if self.roles else None)
        else:
            await self.message.edit(view=self.view if self.roles else None)

    def remove_deleted(self):
        """Remove all roles on this message which have since been deleted"""
        guild = self.message.guild
        if not guild:
            return
        for role in [*self.roles.keys()]:
            if not guild.get_role(role):
                del self.roles[role]

    def add_role(
        self,
        role: discord.Role,
        emoji: str | None = None,
        label: str = MISSING,
        row: Literal[1, 2, 3, 4, 5] | None = None,
        role_type: Literal["add", "remove", "both"] = "both",
        style: discord.ButtonStyle = discord.ButtonStyle.grey,
        description: str = MISSING,
    ) -> None:
        if role.id in self.roles:
            raise ValueError("role has already been added")
        if len(self.roles) >= 25:
            raise ValueError("this message is maxed at 25 roles")

        self.roles[role.id] = SelfAssignRole(
            emoji=emoji if emoji is not MISSING else None,
            label=label or role.name,
            row=row,
            type=role_type,
            style=style.name,  # noqa
            id=role.id,
            description=description if description is not MISSING else None,
        )

    def remove_role(self, role: discord.Role) -> None:
        if role.id not in self.roles:
            raise ValueError("role does not exist on this message")
        del self.roles[role.id]

    def edit_role(
        self,
        role: discord.Role,
        emoji: str | None = MISSING,
        label: str = MISSING,
        row: Literal[1, 2, 3, 4, 5] | None = MISSING,
        role_type: Literal["add", "remove", "both"] = MISSING,
        style: ButtonStyleType = MISSING,
        description: str = MISSING,
    ):
        if role.id not in self.roles:
            raise ValueError("cannot edit role as it does not exist on this message")

        update = {}
        if emoji is not MISSING:
            update["emoji"] = emoji
        if label is not MISSING:
            update["label"] = label
        if row is not MISSING:
            update["row"] = row
        if role_type is not MISSING:
            update["type"] = role_type
        if style is not MISSING:
            update["style"] = style
        if description is not MISSING:
            update["description"] = description
        self.roles[role.id].update(update)
