import typing
from typing import Literal, Type

import aiohttp
import discord
from discord import app_commands

from argon import commands, db
from argon.bot import Argon
from argon.cogs.selfassign.message_types import RoleMessage
from argon.cogs.selfassign.shared import _, log
from argon.utils import MISSING, slash_guild
from argon.utils.chat_formatting import error, success, warning

ButtonStyleType = Literal["blurple", "grey", "green", "red"]
RoleType = Literal["add", "remove", "both"]


class WebhookNotFound(Exception):
    pass


if typing.TYPE_CHECKING:
    RoleMessageConverter = discord.Message
    PartialRoleMessageConverter = discord.PartialMessage
else:

    class PartialRoleMessageConverter(commands.PartialMessageConverter):
        async def convert(self, ctx: commands.Context, argument: str) -> discord.PartialMessage:
            value = await super().convert(ctx, argument)
            # noinspection PyTypeChecker
            cog: SelfAssign = ctx.bot.get_cog("SelfAssign")
            # noinspection PyProtectedMember
            await cog._load_role_message(value.id)
            return value

    class RoleMessageConverter(commands.MessageConverter):
        async def convert(self, ctx: commands.Context, argument: str) -> discord.Message:
            value = await super().convert(ctx, argument)
            # noinspection PyTypeChecker
            cog: SelfAssign = ctx.bot.get_cog("SelfAssign")
            # noinspection PyProtectedMember
            await cog._load_role_message(value.id)
            return value


class SelfAssign(commands.Cog):
    def __init__(self, bot: Argon):
        super().__init__()
        self.bot = bot
        self._role_messages: dict[int, RoleMessage] = {}
        self._cannot_load: set[int] = set()
        self._session = aiohttp.ClientSession()

    def cog_unload(self) -> None:
        for msg in self._role_messages.values():
            msg.view.stop()
        self.bot.loop.create_task(self._session.close())

    async def _load_role_message(self, mid: int) -> RoleMessage | None:
        if mid in self._cannot_load:
            return None

        if mid not in self._role_messages:
            data: db.Message | None = await db.Message.find_one({"message_id": mid})
            if not data:
                # Might be a temporary failure, such as adding a first role to a new message;
                # as such, we aren't going to add the message to _cannot_load in this case.
                return None
            try:
                cls: Type[RoleMessage] = RoleMessage.MESSAGE_TYPES[data.role_type]
            except KeyError:
                log.error(
                    "Cannot load role message %s: %r is not a valid role message type",
                    mid,
                    data.role_type,
                )
                self._cannot_load.add(mid)
                return None
            try:
                self._role_messages[data.message_id] = cls.load(self.bot, data)
            except Exception as e:
                log.error("Failed to load message %s", mid, exc_info=e)
                self._cannot_load.add(mid)
                return None
            log.debug("Loaded %r from database", data)

        return self._role_messages[mid]

    async def _create_role_message(self, mid: int, data: db.Message) -> RoleMessage:
        if rm := self._role_messages.pop(mid, None):
            rm.view.stop()
        type_cls: Type[RoleMessage] = RoleMessage.MESSAGE_TYPES[data.role_type]
        m = self._role_messages[mid] = type_cls.load(self.bot, data)
        return m

    @commands.Cog.listener()
    async def on_interaction(self, interaction: discord.Interaction):
        # This is being done in order to avoid loading every role message into memory on bot startup
        # The consequence of this is that we don't get a lot of the fancier view handling d.py
        # gives us, but that's something I'm fine with if it means we can save on a bit of memory
        # in large guilds, and possibly improve time from startup to being ready for use.

        # Make sure this is a button interaction
        if interaction.type is not discord.InteractionType.component or not interaction.data:
            return
        custom_id: str = interaction.data.get("custom_id")
        # Ensure this is a role button
        if custom_id is None or not custom_id.startswith("role_"):
            return
        log.debug("Got role message interaction with custom ID %r", custom_id)
        if any(
            discord.utils.get(view.children, custom_id=custom_id)
            for view in self.bot.persistent_views
        ):
            log.debug("Interaction has a persistent view, bailing")
            return

        # Select menus use 'role_{message_id}_openselect', but button roles use
        # 'role_{message_id}__{role_id}'
        mid = int(custom_id.split("_")[1])
        msg = await self._load_role_message(mid)
        if not msg:
            log.warning("Failed to find data for role button interaction on message %r", mid)
            return

        button = discord.utils.get(msg.view.children, custom_id=custom_id)
        if button is None:
            return
        log.debug("Found button %r", button)
        # noinspection PyUnresolvedReferences
        await button.callback(interaction)

    def _can_modify(self, message: discord.Message) -> bool:
        if isinstance(message, discord.PartialMessage):
            return True  # just assume true on partial messages
        if message.author == self.bot.user:
            return True
        return False

    @commands.hybrid_group(aliases=["buttonrole", "brole"])
    @commands.guild_only()
    @commands.guild_slash_permissions(manage_roles=True, manage_channels=True)
    @commands.bot_has_guild_permissions(manage_roles=True)
    @slash_guild
    async def selfrole(self, ctx: commands.Context):
        """Add buttons to messages which give members roles when used"""
        if not ctx.invoked_subcommand:
            await ctx.send_help()

    @selfrole.group(name="message")
    async def brole_message(self, ctx: commands.Context):
        """Modify how members get roles from a button role message"""

    @brole_message.command(name="type")
    @app_commands.describe(
        message="A link to the message to modify", message_type="The style to use for this message"
    )
    async def brole_type(
        self,
        ctx: commands.Context,
        message: PartialRoleMessageConverter,
        message_type: Literal["button", "select"],
    ):
        """Select which type of role selection a given message uses

        `button` just places buttons on the message itself which users can click
        to add and/or remove roles from themselves, whereas `select` instead only
        adds a button which then sends a select menu with the added roles when clicked.

        For messages with multiple roles, `select` may be preferred, as it
        makes it much clearer as to which roles someone has and doesn't have.
        """
        data = await db.Message.find_one_or_create(message)
        await data.set({db.Message.role_type: message_type})
        if message.id in self._role_messages:
            # Recreate the message in question
            await (await self._create_role_message(message.id, data)).update()
        await ctx.send(
            _(
                "Updated message to use {type, select, button {buttons} select {a select menu}"
                " other {an unknown message type?}} for role selection"
            ).format(type=message_type)
        )

    @brole_message.command(name="exclusive")
    @app_commands.describe(
        message="A link to the message to modify",
        exclusive="Whether the roles on this message are mutually exclusive or not",
    )
    async def brole_exclusive(
        self, ctx: commands.Context, message: PartialRoleMessageConverter, exclusive: bool = None
    ):
        """Toggle if roles on a message are exclusive"""
        data = await db.Message.find_one_or_create(message)
        exclusive = not data.mutually_exclusive if exclusive is None else exclusive
        await data.set({"mutually_exclusive": exclusive})
        if m := self._role_messages.get(message.id):
            m.mutually_exclusive = exclusive

        if exclusive:
            await ctx.send(_("Roles on that message are now mutually exclusive.").format())
        else:
            await ctx.send(_("Roles on that message are no longer mutually exclusive.").format())

    @brole_message.command(name="button")
    @app_commands.describe(
        message="A link to the message to modify",
        emoji="An emoji to use on the button",
        style="The button style",
        text="The button text",
    )
    async def message_buttontext(
        self,
        ctx: commands.Context,
        message: PartialRoleMessageConverter,
        style: ButtonStyleType,
        emoji: str,
        text: str,
    ):
        """Modify the button text on a select-style message"""
        # Avoid creating a database entry if we don't need to
        data = await db.Message.find_one(
            {"channel_id": message.channel.id, "message_id": message.id}
        )
        if not data or data.role_type != "select":
            return await ctx.send(_("That message is not a select-style message").format())

        await data.set(
            {
                "select_button": {
                    "message": text,
                    "emoji": emoji or None,
                    "style": style,
                }
            }
        )
        if mr := self._role_messages.get(message.id):
            mr.reload(self.bot, data)
            await mr.update()
        await ctx.send(_("Updated button successfully.").format())

    @brole_message.command(name="emojis")
    @app_commands.describe(
        add="The emoji to use to indicate added roles",
        remove="The emoji to use to indicate removed roles",
    )
    async def select_addremove_emojis(self, ctx: commands.Context, add: str, remove: str):
        """Change the emojis used when denoting added/removed roles in selects"""
        data = await db.Guild.find_one_or_create(ctx.guild)
        # There's technically no restrictions on what can be used as 'emojis',
        # since this is just plain text used in a content response; as such,
        # even strings such as '+' and '-' are accepted (albeit not recommended).
        await data.set({"select_emojis": {"add": add, "remove": remove}})
        await ctx.send(_("Add/remove emojis updated.").format())

    @staticmethod
    async def _add_role(
        ctx: commands.Context,
        role_msg: RoleMessage,
        out: list[str],
        role: discord.Role,
        style: ButtonStyleType = "grey",
        row: commands.Range[int, 1, 5] = None,
        emoji: str = None,
        label: str = None,
        description: str = None,
        role_type: RoleType = "both",
    ) -> bool:
        role_msg.remove_deleted()

        if len(role_msg.roles) >= 25:
            out.append(error(_("You cannot add more than 25 roles to a single message.").format()))
            return False
        if not role.is_assignable():
            out.append(
                error(_("I cannot assign and/or remove {role} from members.").format(role=role))
            )
            return False

        if role in role_msg.all_roles():
            out.append(
                warning(
                    _(
                        "{role} has already been added to this message; use"
                        " `{prefix}brole edit` instead"
                    ).format(role=role, prefix=ctx.clean_prefix)
                )
            )
            return False

        role_msg.add_role(
            role=role,
            emoji=emoji or None,
            label=label or role.name,
            row=row,
            role_type=role_type or "both",
            # this handles MISSING by just using the default
            style=getattr(discord.ButtonStyle, str(style), discord.ButtonStyle.grey),
            description=description or None,
        )
        return True

    # This would be a good candidate for a slash-only command, but to rewrite this to be
    # slash only is simply more effort than I want to put into this.
    @selfrole.command(name="add", aliases=["link"])
    @app_commands.describe(
        message="A link to the message to add this role to",
        role="The role to add",
        style="The button style to use",
        row="The row to place this button on, max 5 roles per row; not applicable for select menus",
        emoji="The emoji to use for this role",
        label="The label for this role",
        description="The description for this role; only applicable on select menus",
        role_type="Whether members can only add or remove this role; defaults to both",
    )
    async def brole_link(
        self,
        ctx: commands.Context,
        message: RoleMessageConverter,
        role: discord.Role,
        style: ButtonStyleType = "grey",
        row: commands.Range[int, 1, 5] = None,
        emoji: str = None,
        label: str = None,
        description: str = None,
        role_type: RoleType = "both",
    ):
        """Link a role to a given message

        The given message must have been created by the bot itself; an option for doing
        so is with `[p]discohook send`.

        Due to Discord limitations, messages may only have 25 total roles.

        **Note:** This command is better used as a slash command (`/selfrole add`).
        """
        if not self._can_modify(message):
            await ctx.send(
                error(
                    _(
                        "That message wasn't created by myself; try making one"
                        " with `{prefix}embed`."
                    ).format(prefix=ctx.clean_prefix)
                )
            )
            return
        if role >= ctx.author.top_role:
            await ctx.send(
                _("You cannot modify that role due to this server's role hierarchy.").format()
            )
            return

        msg = await db.Message.find_one_or_create(message)
        if message.id not in self._role_messages:
            if message.author.discriminator == "0000" and msg.is_webhook is None:
                await msg.set({"is_webhook": message.author.id})
            try:
                role_msg = await self._create_role_message(message.id, msg)
            except WebhookNotFound:
                await ctx.send(
                    error(
                        _(
                            "This message was sent by a webhook which either I do not manage or"
                            " no longer exists."
                        ).format()
                    )
                )
                return
        else:
            role_msg = self._role_messages[message.id]
        out = []
        added = await self._add_role(
            ctx,
            role_msg,
            out,
            role,
            style=style,
            row=row,
            emoji=emoji,
            label=label,
            description=description,
            role_type=role_type,
        )

        try:
            await role_msg.update()
        except discord.HTTPException as e:
            if e.status == 400:
                role_msg.reload(self.bot, msg)
                return await ctx.send(
                    warning(_("Failed to update message; did you use an invalid emoji?").format())
                )
            raise
        await role_msg.save()

        if added:
            out.append(
                success(_("Added {role} to the given message successfully.").format(role=role))
            )
        if not out:
            out.append(_("Failed to link any roles to this message.").format())
        await ctx.send("\n".join(out))

    @selfrole.command(name="edit")
    @app_commands.describe(
        message="A link to the message being edited",
        role="The role to modify",
        style="The button style to use",
        row="The row to place this button on, max 5 roles per row; not applicable for select menus",
        emoji="The emoji to use for this role",
        label="The label for this role",
        description="The description for this role; only applicable on select menus",
        role_type="Whether members can only add or remove this role; defaults to both",
    )
    async def brole_edit(
        self,
        ctx: commands.Context,
        message: PartialRoleMessageConverter,
        role: discord.Role,
        style: ButtonStyleType = None,
        row: commands.Range[int, 1, 5] = None,
        emoji: str = None,
        label: str = None,
        description: str = None,
        role_type: RoleType = None,
    ):
        """Modify an existing button for a role

        Any arguments not specified will have their current value preserved.
        """
        role_msg = self._role_messages.get(message.id)
        if not role_msg or not role_msg.roles:
            await ctx.send(warning(_("That message doesn't have any roles setup.").format()))
            return
        if role >= ctx.author.top_role:
            await ctx.send(
                _("You cannot modify that role due to this server's role hierarchy.").format()
            )
            return

        role_msg.edit_role(
            role,
            emoji=emoji or MISSING,
            label=label or MISSING,
            row=row or MISSING,
            role_type=role_type or MISSING,
            style=style or MISSING,
            description=description or MISSING,
        )
        try:
            await role_msg.update()
        except discord.HTTPException as e:
            if e.code == 400:
                role_msg.reload(self.bot, await db.Message.find_one_or_create(message))
                return await ctx.send(
                    warning(_("Failed to update message; did you use an invalid emoji?").format())
                )
            raise
        await role_msg.save()
        await ctx.send(
            success(_("Updated {role} on the given message successfully.").format(role=role))
        )

    @selfrole.command(name="remove", aliases=["unlink"])
    @app_commands.describe(
        message="A link to the message to remove this role from", role="The role to remove"
    )
    async def brole_unlink(
        self, ctx: commands.Context, message: RoleMessageConverter, role: discord.Role
    ):
        """Remove a role from a button role message"""
        role_msg = self._role_messages.get(message.id)
        if not role_msg or not role_msg.roles:
            await ctx.send(warning(_("That message doesn't have any roles setup.").format()))
            return
        if role >= ctx.author.top_role:
            await ctx.send(
                _("You cannot modify that role due to this server's role hierarchy.").format()
            )
            return

        if role.id not in role_msg.roles:
            await ctx.send(warning(_("This role isn't configured on that message.").format()))
            return

        role_msg.remove_role(role)
        role_msg.remove_deleted()
        await role_msg.update()
        await role_msg.save()
        await ctx.send(success(_("Removed role {role} from the given message.").format(role=role)))

    @selfrole.command(name="clear")
    @app_commands.describe(message="A link to the message to clear roles on")
    async def brole_clear(self, ctx: commands.Context, message: RoleMessageConverter):
        """Remove all configured roles on a message"""
        role_msg = self._role_messages.pop(message.id, None)
        if not role_msg or not role_msg.roles:
            await ctx.send(warning(_("That message doesn't have any roles setup.").format()))
            return

        role_msg.view.stop()
        data = await db.Message.find_one_or_create(message)
        await data.set({"roles": []})
        if role_msg.webhook:
            await role_msg.webhook.edit_message(message.id, view=None)
        else:
            await message.edit(view=None)
        await ctx.send(success(_("Cleared all roles on that message.").format()))
