import os

LOAD_EXTS = [
    "argon.jishaku",
    "argon.cogs.core",
    "argon.cogs.discohook",
    "argon.cogs.alias",
    "argon.cogs.overseer",
    "argon.cogs.status",
    "argon.cogs.roles",
    "argon.cogs.selfassign",
    "argon.cogs.reminders",
    "argon.cogs.mod",
    "argon.cogs.utils",
]

if "LOAD_DEBUG_COG" in os.environ:
    LOAD_EXTS.append("argon.cogs.debug")

if "ARGON_GIMMICKY" in os.environ:
    LOAD_EXTS.append("argon.cogs.gimmicks")
