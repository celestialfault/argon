from collections import defaultdict, deque
from typing import DefaultDict, Deque

import discord.abc

from argon import commands, db
from argon.bot import Argon
from argon.cogs.mod._shared import _
from argon.cogs.mod.actions import ModActions
from argon.cogs.mod.modlog import ModLog
from argon.cogs.mod.stickerspam import AntiStickerSpam
from argon.cogs.mod.timers import Timers
from argon.cogs.mod.uinfo import UserInfo
from argon.utils import deduplicate
from argon.utils.scheduler import Scheduler


@_.cog()
class Mod(commands.Cog, UserInfo, ModLog, Timers, ModActions, AntiStickerSpam):
    """Moderation utilities"""

    def __init__(self, bot: Argon):
        self.bot = bot
        self._timing_out: set[int] = set()
        # {member: deque([message id, ...])}
        self._sticker_spam: DefaultDict[discord.Member, Deque[int]] = defaultdict(
            lambda: deque(maxlen=20)
        )
        self.scheduler = Scheduler()
        super().__init__()

    def cog_unload(self):
        self._expire_punishment_loop.stop()

    # noinspection PyMethodMayBeStatic
    async def get_names_and_nicks(self, member: discord.abc.User) -> tuple[list[str], list[str]]:
        """Get all previous names and nicknames for a given user

        Returns
        --------
        tuple[list[str], list[str]]
            A tuple containing the list of names and nicknames for a previous user respectively.
            If the given user is not a :class:`discord.Member`, the second list will
            be empty.
        """
        if isinstance(member, discord.Member):
            mdata = await db.Member.find_one_or_create(member)
            nicks = mdata.previous_nicks
        else:
            nicks = []
        udata = await db.User.find_one_or_create(member)
        # noinspection PyRedundantParentheses
        return (udata.previous_names, nicks)

    @commands.Cog.listener("on_member_update")
    async def _member_change_name_log(self, before: discord.Member, after: discord.Member):
        if before.name != after.name:
            user = await db.User.find_one_or_create(after)
            await user.set(
                {
                    "previous_names": deduplicate(
                        [*user.previous_names, after.name], preserve_last=True
                    )[-10:]
                }
            )
        if isinstance(after, discord.Member) and before.nick != after.nick and after.nick:
            member = await db.Member.find_one_or_create(after)
            await member.set(
                {
                    "previous_nicks": deduplicate(
                        [*member.previous_nicks, after.nick][-10:], preserve_last=True
                    )
                }
            )
