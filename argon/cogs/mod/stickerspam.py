from typing import DefaultDict, Deque

import discord

from argon import commands, db
from argon.bot import Argon


class AntiStickerSpam:
    _sticker_spam: DefaultDict[discord.Member, Deque[int]]
    bot: Argon

    @commands.Cog.listener("on_message")
    async def join_sticker_spam_listener(self, message: discord.Message):
        # Basic sanity checks
        if not message.reference or not message.stickers or not message.guild:
            return

        # Make sure we have manage messages
        guild = message.guild
        channel = message.channel
        perms = channel.permissions_for(guild.me)
        if not perms.manage_messages:
            return

        # Check that the message being replied to is a join message
        reply = message.reference.resolved
        if not reply or reply.type != discord.MessageType.new_member:
            return

        # And make sure the guild in question wants sticker spam to be prevented
        # noinspection PyProtectedMember
        gconf: db.Guild = await db.Guild.find_one_or_create(guild)
        if not gconf.sticker_spam:
            return

        member = message.author
        if reply.id in self._sticker_spam[member]:
            await message.delete()
        else:
            self._sticker_spam[member].append(reply.id)
