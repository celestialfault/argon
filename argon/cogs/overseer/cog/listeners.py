import asyncio
from contextlib import suppress
from functools import wraps
from typing import Sequence

import discord
from discord.raw_models import RawBulkMessageDeleteEvent

from argon import commands
from argon.bot import Argon
from argon.cogs.overseer import modules
from argon.cogs.overseer.config import GuildConfig

_messages_deleted: list[discord.Message] = []


def can_log(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        return await func(*args, **kwargs)

    return wrapper


class OverseerEvents:
    bot: Argon

    @commands.Cog.listener()
    @can_log
    async def on_filter_message_delete(self, message: discord.Message, hits: Sequence[str]):
        with suppress(KeyError):
            _messages_deleted.remove(message)
        await modules.message.filtered(message, hits)

    @commands.Cog.listener()
    @can_log
    async def on_thread_join(self, thread: discord.Thread):
        await modules.channel.thread_create(thread)

    @commands.Cog.listener()
    @can_log
    async def on_thread_delete(self, thread: discord.Thread):
        await modules.channel.thread_delete(thread)

    @commands.Cog.listener()
    @can_log
    async def on_thread_update(self, before: discord.Thread, after: discord.Thread):
        await modules.channel.thread_update(before, after)

    @commands.Cog.listener()
    @can_log
    async def on_message_delete(self, message: discord.Message):
        # This is probably susceptible to race conditions, but the scope of
        # what this would impact is relatively small and inconsequential (just
        # a few more log messages), so I'm not worried about fixing it.
        _messages_deleted.append(message)
        await asyncio.sleep(0.4)
        try:
            _messages_deleted.remove(message)
        except ValueError:
            # either bulk deleted or filtered; discard the event
            return

        if getattr(message, "guild", None) is None:
            return
        await modules.message.delete(message)

    @commands.Cog.listener()
    @can_log
    async def on_message_edit(self, before: discord.Message, after: discord.Message):
        if not after.guild:
            return
        await modules.message.edit(before, after)

    @commands.Cog.listener()
    @can_log
    async def on_raw_bulk_message_delete(self, payload: RawBulkMessageDeleteEvent):
        for mid in payload.message_ids:
            with suppress(ValueError, StopIteration):
                _messages_deleted.remove(next(iter(x for x in _messages_deleted if x.id == mid)))

        channel: discord.abc.Messageable = self.bot.get_channel(payload.channel_id)
        if not getattr(channel, "guild", None):
            return
        await modules.message.bulk_delete(channel, payload.message_ids)

    @commands.Cog.listener()
    @can_log
    async def on_member_join(self, member: discord.Member):
        await modules.member.join(member)

    @commands.Cog.listener()
    @can_log
    async def on_member_remove(self, member: discord.Member):
        await modules.member.leave(member)

    @commands.Cog.listener()
    @can_log
    async def on_member_update(self, before: discord.Member, after: discord.Member):
        await modules.member.update(before, after)

    @commands.Cog.listener()
    @can_log
    async def on_guild_channel_create(self, channel: discord.abc.GuildChannel):
        await modules.channel.create(channel)

    @commands.Cog.listener()
    @can_log
    async def on_guild_channel_delete(self, channel: discord.abc.GuildChannel):
        await modules.channel.delete(channel)

    @commands.Cog.listener()
    @can_log
    async def on_guild_channel_update(
        self, before: discord.abc.GuildChannel, after: discord.abc.GuildChannel
    ):
        await modules.channel.update(before, after)

    @commands.Cog.listener()
    @can_log
    async def on_voice_state_update(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if not getattr(member, "guild", None):
            return
        await modules.voice.update(member, before, after)

    @commands.Cog.listener()
    @can_log
    async def on_guild_role_create(self, role: discord.Role):
        await modules.role.create(role)

    @commands.Cog.listener()
    @can_log
    async def on_guild_role_delete(self, role: discord.Role):
        await modules.role.delete(role)

    @commands.Cog.listener()
    @can_log
    async def on_guild_role_update(self, before: discord.Role, after: discord.Role):
        await modules.role.update(before, after)

    @commands.Cog.listener()
    @can_log
    async def on_guild_update(self, before: discord.Guild, after: discord.Guild):
        await modules.guild.update(before, after)

    @commands.Cog.listener()
    @can_log
    async def on_guild_emojis_update(
        self, guild: discord.Guild, before: list[discord.Emoji], after: list[discord.Emoji]
    ):
        await modules.guild.guild_emoji.in_context(guild, before, after)

    @commands.Cog.listener()
    async def on_guild_leave(self, guild: discord.Guild):
        GuildConfig.remove_from_cache(guild)

    @commands.Cog.listener()
    async def on_personal_role_name(self, *args, **kwargs):
        await modules.role.personal_name(*args, **kwargs)

    @commands.Cog.listener()
    async def on_personal_role_colour(self, *args, **kwargs):
        await modules.role.personal_colour(*args, **kwargs)
