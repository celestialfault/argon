"""Basic Event and Module classes for use in typing without using circular imports"""

from __future__ import annotations

import abc
from contextvars import ContextVar
from typing import Any, AsyncIterable, Awaitable, Callable, Iterable, TypeVar, Union

import discord

from argon.translations import LazyStr

__all__ = (
    "ModuleABC",
    "EventABC",
    "NoGuildException",
    "ARGUMENTS",
    "LogType",
    "Loggable",
    "CheckCallable",
    "SkipEvent",
)
LogType = TypeVar("LogType", str, discord.Embed, tuple, dict)
Loggable = Union[LogType, Awaitable[LogType], AsyncIterable[LogType], Iterable[LogType]]
CheckCallable = Callable[..., Union[bool, Awaitable[bool]]]
ARGUMENTS: ContextVar[tuple[tuple, dict]] = ContextVar("arguments")


class NoGuildException(Exception):
    pass


class SkipEvent(Exception):
    """Utility exception that can be raised at any point while parsing an event

    If this is raised, the current event invocation is completely ignored and nothing
    will be logged, even if output has already been yielded. This exception is
    silently discarded.

    This is most useful in downstream code, such as for example :class:`AuditHandler`.
    """


class EventABC(abc.ABC):
    """Event base class

    This class is used for typings to avoid circular imports; the main class is
    :class:`overseer.event.Event`.

    Arguments
    ---------
    event_id: str
        The internal event ID; this is also used when referring to this event in commands,
        such as with ``[p]overseer (list|set)``
    friendly_name: str | LazyStr
        The friendly name used in ``[p]overseer list``
    callable: Callable
        The decorated event parser callable; this should return or yield log messages.
        The callable may be sync or async.
    module: ModuleABC
        The Module that this event belongs to
    description: str | LazyStr
        Optional event description used in ``[p]overseer list``. This may be omitted.
    parent: EventABC
        Which event this event is a child of. This may be omitted.
    checks: list[CheckCallable]
        A list of pre-defined check callables; this is an alternative to using :meth:`Module.check`.
        This may be omitted.
    mutually_exclusive: bool
        Whether this event is a mutually exclusive event group or not. Events considered mutually
        exclusive must have at least one child event, and will only ever call one when called,
        even if multiple child events are enabled.
    priority: float
        The priority in which this event will be called. This is best used with a mutually
        exclusive parent event, as there's no guarantee that events with a higher priority
        will actually log before other events. Lower numbers are prioritized before higher numbers.
        Child events of mutually exclusive events default to a priority of `inf`, meaning
        that events with a priority set will by default be preferred over ones that don't.
    requires_features: set[str]
        A set of :attr:`discord.Guild.features` values that the current guild must have for
        this event to function.
    """

    __slots__ = (
        "event_id",
        "callable",
        "module",
        "description",
        "parent",
        "checks",
        "mutually_exclusive",
        "priority",
        "children",
        "requires_features",
    )

    # noinspection PyShadowingBuiltins
    def __init__(
        self,
        *,
        event_id: str,
        friendly_name: str | LazyStr = ...,
        callable: Callable,  # pylint:disable=redefined-builtin
        module: ModuleABC,
        description: str | LazyStr | None = None,
        parent: EventABC | None = None,
        checks: list[CheckCallable] | None = None,
        mutually_exclusive: bool = False,
        priority: int | float = 0,
        requires_features: set[str] | None = None,
    ):
        self.event_id = event_id
        self.friendly_name = (
            friendly_name
            if friendly_name is not ...
            else " ".join(self.event_id.split("_")).title()
        )
        self.callable = callable
        self.module = module
        # yes, because type documentation of the exact same type is somehow different,
        # thank you pycharm.
        # noinspection PyTypeChecker
        self.description = description
        self.parent = parent
        self.checks = checks or []
        self.mutually_exclusive = mutually_exclusive
        self.priority = priority
        self.requires_features: set[str] = requires_features or set()
        self.children: dict[str, EventABC] = {}

    def __repr__(self):
        return f"<Event id={self.full_id!r} module={self.module!r}>"

    def __str__(self):
        return f"{self.module.id}.{self.full_id}"

    def __hash__(self):
        return id(self)

    @abc.abstractmethod
    def __call__(self, *args, **kwargs) -> None:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def all_parents(self) -> list[EventABC]:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def full_id(self) -> str:
        raise NotImplementedError

    @abc.abstractmethod
    def has_required_features(self, guild: discord.Guild) -> bool:
        """Check if the given guild has all the required features"""
        raise NotImplementedError

    @abc.abstractmethod
    def event(self, event_id: str, **kwargs) -> Callable[[Callable[..., Loggable]], EventABC]:
        raise NotImplementedError

    @abc.abstractmethod
    def ghost(self) -> EventABC:
        raise NotImplementedError

    @abc.abstractmethod
    def resolve(self, cfg) -> list[EventABC]:
        raise NotImplementedError

    @abc.abstractmethod
    async def in_context(self, context_guild: discord.Guild, *args, **kwargs) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def walk(self) -> Iterable[EventABC]:
        raise NotImplementedError


class ModuleABC(abc.ABC):
    """Module base class

    This class is used for typings to avoid circular imports; the main class is
    :class:`overseer.module.Module`.

    Arguments
    ---------
    id: str
        The event ID used to refer to this module in commands
    name: Union[str, LazyStr]
        A human-friendly name used for this module in ``[p]overseer list``
    description: Union[str, LazyStr]
        Optional description used for this module in ``[p]overseer list``
    secret: bool
        Whether this module is visible in ``[p]overseer list``; the bot owner may still see
        secret modules with ``[p]overseer list secret``. This doesn't prevent users
        from using this module, but is instead used to hide modules that wouldn't be useful
        to most users (such as with the built-in ``debug`` module)
    """

    __slots__ = ("id", "name", "description", "secret", "events")

    # noinspection PyShadowingBuiltins
    def __init__(
        self,
        id: str,  # pylint:disable=redefined-builtin
        name: str | LazyStr,
        *,
        description: str | LazyStr | None = None,
        secret: bool = False,
    ):
        self.id = id
        # noinspection PyTypeChecker
        self.name = name
        # noinspection PyTypeChecker
        self.description = description
        self.secret = secret
        self.events: list[EventABC] = []

    def __repr__(self):
        return f"<Module id={self.id!r}>"

    def __hash__(self):
        return hash(self.id)

    @abc.abstractmethod
    def event(self, event_id: str, **kwargs) -> Callable[[Callable[..., Loggable]], EventABC]:
        raise NotImplementedError

    @abc.abstractmethod
    def ghost(self) -> Callable[[Callable[..., Any]], EventABC]:
        raise NotImplementedError

    @staticmethod
    @abc.abstractmethod
    def check(predicate: CheckCallable):
        raise NotImplementedError

    @abc.abstractmethod
    def walk(self) -> Iterable[EventABC]:
        raise NotImplementedError
