import asyncio
import logging
import warnings
from datetime import datetime
from enum import IntEnum

import discord

from argon import commands, db
from argon._shared import _
from argon.utils import deduplicate

log = logging.getLogger(__name__)


class ExitCode(IntEnum):
    NORMAL = 0
    FATAL_ERROR = 1
    CONFIG_ERROR = 38


# noinspection PyAbstractClass
class Argon(commands.Bot):
    def __init__(self, *args, **kwargs):
        self.__command_dict = {}
        self.argon_ready = asyncio.Event()
        super().__init__(command_prefix=self.prefixes, *args, **kwargs)
        self.uptime: datetime | None = None
        self._exit_code: int = ExitCode.NORMAL
        self._db_cache = {}
        self.bot_config: db.Bot = ...
        self.allowed_mentions = discord.AllowedMentions.none()
        self.get_command("help").hidden = True

    async def setup_hook(self):
        # Application command sync cannot be done here as the application ID has not yet
        # been loaded in this step of the setup process
        self.uptime = datetime.utcnow()
        self.bot_config = await db.Bot.find_one_or_create()

    @property
    def command_dict(self) -> dict[str, commands.Command]:
        """A pre-flattened dict of all text-based bot commands"""
        return self.__command_dict

    @property
    def colour(self) -> discord.Colour | None:
        """The global bot colour to use in embeds"""
        if self.bot_config.colour:
            return discord.Colour(self.bot_config.colour)
        return None

    @property
    def description(self):
        return _("Moderation utility bot").format()

    @description.setter
    def description(self, value):
        # This is required as otherwise super().__init__() will raise an AttributeError
        pass

    def add_command(self, command) -> None:
        self.__command_dict[command.qualified_name] = command
        if isinstance(command, commands.Group):
            self.__command_dict.update({x.qualified_name: x for x in command.walk_commands()})
        super().add_command(command)

    def remove_command(self, name) -> commands.Command | None:
        command = super().remove_command(name)
        if command:
            if isinstance(command, commands.Group):
                for cmd in command.walk_commands():
                    self.__command_dict.pop(cmd.qualified_name, None)
            self.__command_dict.pop(command.qualified_name, None)
        return command

    async def get_context(self, message, *, cls=commands.Context) -> commands.Context:
        return await super().get_context(message, cls=cls)

    async def process_commands(
        self, message: discord.Message, *, dispatch_without_command: bool = True
    ):
        if not message.author.bot:
            ctx = await self.get_context(message)
            await self.invoke(ctx)
        else:
            ctx = None

        if ctx is None or ctx.valid is False and dispatch_without_command:
            if "Alias" in self.cogs:
                # alias will dispatch a message_without_command event if this doesn't
                # match any aliases
                # noinspection PyUnresolvedReferences
                await self.get_cog("Alias").process_aliases(message)
            else:
                self.dispatch("message_without_command", message)

    async def prefixes(self, _, message: discord.Message) -> list[str]:
        """Get command prefixes for a given command invocation"""
        await self.wait_until_ready()
        prefixes = self.bot_config.prefixes
        if message.guild:
            # noinspection PyTypeChecker
            guild: db.Guild = await db.Guild.find_one_or_create(message.guild)
            prefixes = guild and guild.prefixes or prefixes
        return deduplicate([f"<@{self.user.id}> ", f"<@!{self.user.id}> ", *prefixes])

    # overridden to make pycharm happy and also accept Member
    async def is_owner(self, user: discord.User | discord.Member) -> bool:
        return await super().is_owner(user)

    @staticmethod
    async def is_blocked(member: discord.User | discord.Member) -> bool:
        """Return if the given member is currently blocked from using bot commands"""
        if (
            isinstance(member, discord.Member)
            and (await db.Member.find_one_or_create(member)).blocked
        ):
            return True
        return (await db.User.find_one_or_create(member)).blocked

    async def wait_until_argon_ready(self) -> None:
        """Deprecated in favour of :meth:`wait_until_ready`

        All critical loading is now done before the discord.py ready status is ever set,
        and as such the reasons for this method are simply legacy uses of it.
        """
        warnings.warn(
            "Argon#wait_until_argon_ready() is deprecated, use #wait_until_ready() instead",
            DeprecationWarning,
            stacklevel=2,
        )
        await self.argon_ready.wait()
        await self.wait_until_ready()
