from datetime import timedelta
from typing import Literal

from beanie import Document

__all__ = ("Bot",)


# This should only contain configuration data that can be changed, and isn't needed for
# bot startup. If you require data to be provided during startup, require it as an
# environment variable instead of placing it here.
class Bot(Document):
    # List of global bot prefixes; this can be overridden on a per-guild basis - see: Guild#prefixes
    prefixes: list[str] = []
    # Default embed colour; currently only used by overseer.
    colour: int | None = None
    activities: list[dict] = []
    # Only applies if at least one activity is set in `activities`
    activity_status: Literal["online", "idle", "dnd", "offline"] = "online"
    # The bot will log a message in this channel once startup is successful
    startup_channel: int | None = None
    # There's currently no exposed way to change this, but that may be changed
    # in the future; until then, the only way to change this is through the following eval:
    #   [p]jsk py await bot.bot_config.set({'locale': '...'})
    # The bot must then be restarted for the change to actually be applied.
    locale: str = "en_US"

    @classmethod
    async def find_one_or_create(cls):
        doc = await cls.find_one()
        if not doc:
            doc = cls()
            await doc.insert()
        return doc

    class Settings:
        name = "bot"
        use_cache = True
        cache_expiration_time = timedelta(days=30)
        cache_capacity = 1
