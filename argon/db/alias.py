from datetime import timedelta
from enum import IntEnum

from beanie import Document

__all__ = ("Alias", "AliasType")


class AliasType(IntEnum):
    TEXT = 0
    COMMAND = 1


class Alias(Document):
    name: str
    guild_id: int
    type: AliasType
    command: str | None = None
    text: str | None = None

    class Settings:
        # this is title case unlike most other document types, mostly because I'm dumb and
        # forgot to add this before pushing this to production, and I can't be bothered writing
        # a migration to fix it.
        name = "Alias"
        use_cache = True
        cache_expiration_time = timedelta(minutes=15)
        cache_capacity = 1000
