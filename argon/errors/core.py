import logging
from typing import Awaitable, Callable, Type

import discord

from argon import commands
from argon._shared import _

log = logging.getLogger("argon.events")
__all__ = ("ErrorDispatch",)


# Yes, singledispatch would be the better option, but that didn't seem to work as intended
# when I tried using it, so we get to use our own custom handling instead.
class ErrorDispatch:
    __slots__ = ("_registry",)

    def __init__(self):
        self._registry: dict[
            Type[Exception], Callable[[Exception, commands.Context], Awaitable[None]]
        ] = {}

    @staticmethod
    async def _default_callback(exc: Exception, ctx: commands.Context):
        msg = _(
            "`An unexpected error has occurred while attempting to process this command.`"
        ).format()
        await ctx.send(msg, ephemeral=True)
        log.exception("Failed to invoke command", exc_info=exc)

    async def __call__(self, exc: Exception, ctx: commands.Context) -> None:
        callback = self._default_callback
        for exc_type, cb in self._registry.items():
            if isinstance(exc, exc_type):
                callback = cb
                break
        try:
            await discord.utils.maybe_coroutine(callback, exc, ctx)
        except Exception as e:
            log.critical(
                "Encountered an exception while handling an exception in command %r",
                ctx.command and ctx.command.qualified_name or "<no command provided>",
                exc_info=e,
            )

    def register(self, *exceptions, func=None):
        # noinspection PyShadowingNames
        def wrapper(func):
            self._registry.update({exc: func for exc in exceptions})
            return func

        if func is None:
            return wrapper
        else:
            return wrapper(func)
