"""
While PyCharm definitely has many issues around correctly inferring types with d.py, I'm far too
used to using it to be able to switch to VS Code over some annoying type hinting issues alone, which
can (relatively) easily be fixed by just including some type hint stubs, like what's being done
here.

(i also kept using ``commands.Interaction`` before remembering it was ``discord.Interaction``
instead)
"""
from typing import TYPE_CHECKING

import discord

# noinspection PyProtectedMember
from discord._types import ClientT

__all__ = ("Interaction",)

if TYPE_CHECKING:
    # noinspection PyPropertyDefinition
    class Interaction(discord.Interaction):
        @property
        def response(self) -> discord.InteractionResponse[ClientT]:
            ...

        @property
        def followup(self) -> discord.Webhook:
            ...

else:
    Interaction = discord.Interaction
