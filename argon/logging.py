"""Logging configuration

A lot of this has been mostly adapted from `Red`_, especially the Rich configuration.

.. _Red:: https://github.com/Cog-Creators/Red-DiscordBot/blob/67e43eb00b/redbot/logging.py
"""
import logging
import pathlib
from datetime import datetime

from rich._log_render import LogRender as _LogRender  # noqa
from rich.console import group
from rich.logging import RichHandler as _RichHandler
from rich.style import Style
from rich.text import Text
from rich.theme import Theme
from rich.traceback import Traceback as _Traceback

__all__ = ("init_logging",)


class Traceback(_Traceback):
    @group()
    def _render_stack(self, stack):
        # noinspection PyUnresolvedReferences
        for obj in super()._render_stack.__wrapped__(self, stack):
            if obj != "":
                yield obj


class LogRender(_LogRender):
    def __call__(
        self,
        console,
        renderables,
        log_time=None,
        time_format=None,
        level="",
        path=None,
        line_no=None,
        link_path=None,
        logger_name=None,
    ):
        output = Text()
        if self.show_time:
            log_time = log_time or console.get_datetime()
            log_time_display = log_time.strftime(time_format or self.time_format)
            if log_time_display == self._last_time:
                output.append(" " * (len(log_time_display) + 1))
            else:
                output.append(f"{log_time_display} ", style="log.time")
                self._last_time = log_time_display
        if self.show_level:
            # The space needs to be added separately so that log level is colored by
            # Rich.
            output.append(level)
            output.append(" ")
        if logger_name:
            output.append(f"[{logger_name}] ", style="bright_black")

        output.append(*renderables)
        if self.show_path and path:
            path_text = Text()
            path_text.append(path, style=f"link file://{link_path}" if link_path else "")
            if line_no:
                path_text.append(f":{line_no}")
            output.append(path_text)
        return output


class RichHandler(_RichHandler):
    """Adaptation of Rich's RichHandler to manually adjust the path to a logger name"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._log_render = LogRender(
            show_time=self._log_render.show_time,
            show_level=self._log_render.show_level,
            show_path=self._log_render.show_path,
            level_width=self._log_render.level_width,
        )

    def get_level_text(self, record: logging.LogRecord) -> Text:
        level_text = super().get_level_text(record)
        level_text.stylize("bold")
        return level_text

    def emit(self, record: logging.LogRecord) -> None:
        """Invoked by logging."""
        path = pathlib.Path(record.pathname).name
        level = self.get_level_text(record)
        message = self.format(record)
        time_format = None if self.formatter is None else self.formatter.datefmt
        log_time = datetime.fromtimestamp(record.created)

        traceback = None
        if self.rich_tracebacks and record.exc_info and record.exc_info != (None, None, None):
            exc_type, exc_value, exc_traceback = record.exc_info
            assert exc_type is not None
            assert exc_value is not None
            traceback = Traceback.from_exception(
                exc_type,
                exc_value,
                exc_traceback,
                width=self.tracebacks_width,
                extra_lines=self.tracebacks_extra_lines,
                theme=self.tracebacks_theme,
                word_wrap=self.tracebacks_word_wrap,
                show_locals=self.tracebacks_show_locals,
                locals_max_length=self.locals_max_length,
                locals_max_string=self.locals_max_string,
                indent_guides=False,
            )
            message = record.getMessage()

        use_markup = getattr(record, "markup") if hasattr(record, "markup") else self.markup
        message_text = Text.from_markup(message) if use_markup else Text(message)
        if self.highlighter:
            message_text = self.highlighter(message_text)
        if self.KEYWORDS:
            message_text.highlight_words(self.KEYWORDS, "logging.keyword")

        self.console.print(
            self._log_render(
                self.console,
                [message_text],
                log_time=log_time,
                time_format=time_format,
                level=level,
                path=path,
                line_no=record.lineno,
                link_path=record.pathname if self.enable_link_path else None,
                logger_name=record.name,
            ),
            soft_wrap=True,
        )
        if traceback:
            self.console.print(traceback)


def init_logging(loggers: dict[str, int]):
    # markup may be enabled on a per-log basis with extra={"markup": True}
    handler = RichHandler(rich_tracebacks=True, markup=False, show_path=False)
    handler.console.push_theme(
        Theme(
            {
                "logging.level.debug": Style(dim=True),
                "logging.level.info": Style(color="blue"),
                "logging.level.warning": Style(color="yellow"),
                "logging.level.error": Style(color="red"),
                "logging.level.critical": Style(color="white", bgcolor="red"),
            }
        )
    )
    formatter = logging.Formatter("{message}", datefmt="[%X]", style="{")
    handler.setFormatter(formatter)
    root_log = logging.getLogger()
    root_log.addHandler(handler)

    for name, level in loggers.items():
        logging.getLogger(name).setLevel(level)
