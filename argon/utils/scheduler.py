import asyncio
from dataclasses import dataclass
from datetime import datetime
from typing import Any, Awaitable, Callable, Hashable, Optional, Union

import discord.utils


@dataclass
class Scheduled:
    key: Hashable
    dt: datetime
    args: tuple[Any]
    kwargs: dict[str, Any]
    _handle: Optional[asyncio.TimerHandle] = None

    def start(self, callback: Callable[..., Union[Any, Awaitable[Any]]]):
        """Start the current task with the given callback"""
        loop = asyncio.get_event_loop()
        self._handle = loop.call_later(
            (self.dt - datetime.utcnow()).total_seconds(),
            # for whatever reason, call_later doesn't support async callbacks,
            # so we have to wrap it in a task in order for this to function
            # as intended
            lambda: loop.create_task(
                discord.utils.maybe_coroutine(callback, self.key, *self.args, **self.kwargs)
            ),
        )

    def stop(self):
        """Stop the underlying task handle

        This does **not** remove the task from the scheduler; use :meth:`Scheduler.stop` instead.

        If the underlying task has already been stopped or hasn't been setup,
        this will raise an :class:`AttributeError`.
        """
        self._handle.cancel()
        self._handle = None


class Scheduler:
    def __init__(self, callback: Callable[..., Union[Any, Awaitable[Any]]] = None):
        self.callback = callback
        self.scheduled: dict[Hashable, Scheduled] = {}

    def __contains__(self, item):
        return item in self.scheduled

    def add(self, key: Hashable, dt: datetime, /, *args, **kwargs) -> Scheduled:
        """Add a task to be called at ``dt``

        This internally functions the same as calling :meth:`add_callback` with the scheduler's
        callback.

        If this scheduler was created without a callback, this raises a RuntimeError.
        """
        if self.callback is None:
            raise RuntimeError(
                "cannot use add() without a scheduler callback set; use"
                "'.add_callback(callback, ...)' to add a scheduled task without one"
            )
        return self.add_callback(self.callback, key, dt, *args, **kwargs)

    def add_callback(
        self,
        callback: Callable[..., Union[Any, Awaitable[Any]]],
        key: Hashable,
        dt: datetime,
        /,
        *args,
        **kwargs,
    ):
        """Add a task with a custom callback to be called at ``dt``"""
        if key in self.scheduled:
            raise RuntimeError(f"{key!r} is already scheduled")
        timer = self.scheduled[key] = Scheduled(key=key, dt=dt, args=args, kwargs=kwargs)
        timer.start(callback)
        return timer

    def stop(self, key: Hashable):
        """Remove a given task from the scheduler"""
        self.scheduled.pop(key).stop()

    def stop_all(self):
        """Stop all tasks on the scheduler"""
        for scheduled in self.scheduled.values():
            scheduled.stop()
        self.scheduled.clear()
