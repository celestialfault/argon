from __future__ import annotations

from datetime import timedelta
from typing import TYPE_CHECKING

import discord.utils
from babel.dates import format_timedelta
from discord.ext.commands import BadArgument, Converter

from argon._shared import _
from argon.utils.time.parser import (
    AboveMaxDuration,
    BelowMinDuration,
    NoDuration,
    TimePeriod,
    TimeRepresentation,
)

__all__ = ("FutureTime", "TimeDelta", "BelowMinDuration", "AboveMaxDuration", "NoDuration")


class _TimeDelta(Converter):
    # noinspection PyUnresolvedReferences
    """Timedelta conversion utility

    Example
    -------

    >>> from argon.core.utils.time import TimeDelta
    >>> duration = TimeDelta().from_str("1h10m")

    You may also use this class as a type hint if you'd like discord.py to handle the
    grunt work of conversions for you::

        >>> from argon import commands
        >>> from argon.core.utils.time import TimeDelta
        >>> TimeConverter = TimeDelta().min_time("10 seconds")
        >>> @commands.command()
        ... async def my_cmd(ctx, duration: TimeDelta):
        ...     # duration will be an instance of datetime.timedelta
        ...     # invalid input for duration will result in a BadArgument exception,
        ...     # which is handled by discord.py like any other conversion failure
        ...     await ctx.send(f"Total seconds: {duration.total_seconds()}")
    """

    def __init__(self):
        self._min_time: list[TimeRepresentation] = []
        self._max_time: list[TimeRepresentation] = []
        self._default_period: TimePeriod | None = None

    def __repr__(self):
        default_period = self._default_period
        max_time = TimeRepresentation.reps_to_delta(self._max_time) if self._max_time else None
        min_time = TimeRepresentation.reps_to_delta(self._min_time) if self._min_time else None
        return f"TimeDelta({default_period=}, {max_time=}, {min_time=})"

    def from_str(self, duration: str, *, bypass_restrictions: bool = False) -> timedelta:
        """Resolve an input duration with the converter's current configuration options

        All exceptions this method raises are subclasses of :class:`ValueError`.

        Raises
        -------
        BelowMinDuration
        AboveMaxDuration
        NoDuration
        """
        duration = TimeRepresentation.str_to_delta(
            duration, max_duration=self._max_time, default=self._default_period
        )
        if self._min_time and not bypass_restrictions:
            mint = TimeRepresentation.reps_to_delta(self._min_time)
            if duration < mint:
                raise BelowMinDuration()
        if duration.total_seconds() == 0.0:
            raise NoDuration()
        return duration

    def sync_convert(self, argument: str) -> timedelta:
        try:
            return self.from_str(argument)
        except NoDuration:
            raise BadArgument(
                _("`{input}` is not a valid unit of time").format(
                    input=discord.utils.escape_markdown(argument)
                )
            ) from None
        except BelowMinDuration:
            raise BadArgument(
                _("The given time is below the minimum duration of {delta}").format(
                    delta=format_timedelta(TimeRepresentation.reps_to_delta(self._min_time))
                )
            ) from None
        except AboveMaxDuration:
            raise BadArgument(
                _("The given time is above the maximum duration of {delta}").format(
                    delta=format_timedelta(TimeRepresentation.reps_to_delta(self._max_time)),
                )
            ) from None

    async def convert(self, ctx, argument: str) -> timedelta:
        """Internal method used for discord.py conversion support

        This is largely a different frontend to :meth:`from_str` that throws user input errors
        when applicable.

        A synchronous version of this method is available as :meth:`sync_convert`.
        """
        return self.sync_convert(argument)

    def default_period(self, default_period: str) -> TimeDelta:
        """Set the default time period of this parser

        By default, if no default period is set, any integers without an attached
        time period will be ignored.

        Example
        -------
        >>> TimeDelta().default_period("seconds")
        """
        self._default_period = TimePeriod.find(default_period)
        return self

    def min_time(self, time: str) -> TimeDelta:
        """Set the minimum duration of time that this parser will accept

        This accepts the same format that :meth:`from_str` will accept.

        Example
        --------
        >>> TimeDelta().min_time("1h30m")
        """
        self._min_time = TimeRepresentation.str_to_reps(time)
        return self

    def max_time(self, time: str) -> TimeDelta:
        """Set the maximum duration of time that this parser will accept

        This accepts the same format that :meth:`from_str` will accept.

        Example
        --------
        >>> TimeDelta().max_time("1h30m")
        """
        self._max_time = TimeRepresentation.str_to_reps(time)
        return self


TimeDelta =  timedelta if TYPE_CHECKING else _TimeDelta
# kept for backwards compatibility
FutureTime = TimeDelta
